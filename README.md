![Aeva](./doc/userguide/figures/logo.png)

# Introduction

The [Annotation and Exchange of Virtual Anatomy (aeva)](https://aeva.readthedocs.io/) suite
consists of two applications: aevaCMB (this repository)
and [aeva/Slicer](https://gitlab.kitware.com/aeva/aevaslicer).
The aevaCMB application is an open-source, multi-platform annotation and simulation workflow framework
based on [Computational Model Builder (CMB)](https://www.computationalmodelbuilder.org),
[ParaView](https://www.paraview.org),
the [Visualization Toolkit (VTK)](http://www.vtk.org) and [Qt](https://www.qt.io).
You will find binary packages at
the [aeva downloads](https://data.kitware.com/#collection/5e387f9caf2e2eed3562242e) site.

The project has grown through collaborative efforts between
the [Cleveland Clinic](https://www.lerner.ccf.org/bme/), [Kitware Inc.](https://www.kitware.com/),
and various other government and commercial institutions, and acedemic partners.

Learning Resources
==================

As aeva is developed, we will post more information on using and developing with it.

Reporting Bugs
==============

If you have found a bug:

1. If you have a patch, please read the [CONTRIBUTING.md][] document.

2. Otherwise, please join a discussion on our [discourse site][] and ask
   about the expected and observed behaviors to determine if it is
   really a bug.

3. Finally, if the issue is not resolved by the above steps, open
   an entry in the [aeva issue tracker][].

[discourse site]: https://discourse.kitware.com/c/smtk
[aeva issue tracker]: https://gitlab.kitware.com/aeva/aeva/issues

Contributing
============

See [CONTRIBUTING.md][] for instructions to contribute.

[CONTRIBUTING.md]: CONTRIBUTING.md

Latest Release Notes
====================
Can be found [here](doc/release/aeva-2.0.md).

License
=======

Aeva is distributed under the OSI-approved BSD 3-clause License.
See [License.txt][] for details.

[License.txt]: LICENSE.txt
