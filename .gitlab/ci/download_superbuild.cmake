cmake_minimum_required(VERSION 3.12)

# In order to use a new superbuild, please copy the item from the date stamped
# directory into the `keep` directory. This ensure that new versions will not
# be removed by the uploader script and preserve them for debugging in the
# future.

set(data_host "https://data.kitware.com")

# Determine the tarball to download.
# 20240127 – Catch run-time arc editing from SMTK.
if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "vs2019")
  set(file_item "65b5044abeabb3aa0cbd1327")
  set(file_hash "3f826c182d4c40d67cfd490a040c801964f3acac78e7ba01fab8fd2a48deaf298e6de52a54dc5b29a2110dfda5e7c08126e6747c3fbdd7f543dedfb84256f36a")
elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "macos")
  set(file_item "65b50449beabb3aa0cbd1325")
  set(file_hash "39c25f8e6e46eb8668f2d6a9e2c4a14abb750b6b61fbf6771ae7eff6a01a5480c84d9bd5386015d5ffcd924eb8fd87089187eb2151aafb16d89f42e6329b49ef")
else ()
  message(FATAL_ERROR
    "Unknown build to use for the superbuild")
endif ()

# Ensure we have a hash to verify.
if (NOT DEFINED file_item OR NOT DEFINED file_hash)
  message(FATAL_ERROR
    "Unknown file and hash for the superbuild")
endif ()

# Download the file.
file(DOWNLOAD
  "${data_host}/api/v1/item/${file_item}/download"
  ".gitlab/superbuild.tar.gz"
  STATUS download_status
  EXPECTED_HASH "SHA512=${file_hash}")

# Check the download status.
list(GET download_status 0 res)
if (res)
  list(GET download_status 1 err)
  message(FATAL_ERROR
    "Failed to download superbuild.tar.gz: ${err}")
endif ()

# Extract the file.
execute_process(
  COMMAND
    "${CMAKE_COMMAND}"
    -E tar
    xf ".gitlab/superbuild.tar.gz"
  RESULT_VARIABLE res
  ERROR_VARIABLE err
  ERROR_STRIP_TRAILING_WHITESPACE)
if (res)
  message(FATAL_ERROR
    "Failed to extract superbuild.tar.gz: ${err}")
endif ()
