# aeva/CMB 2.2

aeva/CMB is an application for the annotation and exchange of anatomical models
based on
[ParaView](https://paraview.org/),
[SMTK/CMB](https://computationalmodelbuilder.org/), and
[ITK](https://itk.org).
It is currently focused on annotating unstructured data,
as opposed to aeva/Slicer which is focused on structured (image) data.

## Release notes

This release is a minor bugfix release. It fixes two problems:

+ Importing geometry from an STL file prevented export to a MED file[1].
+ Selections on selections[2] and boolean operations on selections[3] were performed incorrectly
  due to a bug in how global IDs were passed through the system.

## General notes

Please see the [user's guide and tutorials at read-the-docs](https://aeva.readthedocs.io)
for instructions on using aeva.
For our initial release, we targeted [feature selections](https://simtk.org/plugins/moinmoin/aeva-apps/IndividualFeatures)
as a way to mark up [knee anatomy](https://simtk.org/projects/openknee).
Our plans for the future include models of other organs as well as
annotations of different functional areas (e.g., neuron classification and fiber tractography
as opposed to joint kinematics).

[1]: https://gitlab.kitware.com/aeva/aeva/-/issues/199
[2]: https://gitlab.kitware.com/aeva/aeva/-/issues/214
[3]: https://gitlab.kitware.com/aeva/aeva/-/issues/209
