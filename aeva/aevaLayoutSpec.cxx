//=============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=============================================================================
#include "aevaLayoutSpec.h"

#include <QAction>

void aevaLayoutSpec::layoutDockWidget(QMainWindow* mw, QDockWidget* dw)
{
  static bool set_auto_apply = false;
  static QDockWidget* attDW = nullptr;
  static QDockWidget* infoDW = nullptr;
  static QDockWidget* propDW = nullptr;
  {
    if (dw->objectName() == "pqSMTKAttributePanel")
    {
      dw->setVisible(true);
      mw->resizeDocks({ dw }, { 200 }, Qt::Horizontal);
      attDW = dw;
    }
    else if (dw->objectName() == "pqSMTKResourcePanel")
    {
      // move to the right side.
      mw->removeDockWidget(dw);
      mw->addDockWidget(Qt::RightDockWidgetArea, dw);
      dw->setVisible(true);
    }
    else if (dw->objectName() == "pqSMTKOperationPanel")
    {
      // move to the right side.
      mw->removeDockWidget(dw);
      mw->addDockWidget(Qt::RightDockWidgetArea, dw);
      dw->setVisible(false);
    }
    else if (dw->objectName() == "proxyTabDock")
    {
      dw->setVisible(true);
      propDW = dw;
    }
    else if (dw->objectName() == "informationDock")
    {
      dw->setVisible(true);
      infoDW = dw;
    }
    else if (dw->objectName() == "outputWidgetDock")
    {
      dw->setVisible(true);
      mw->resizeDocks({ dw }, { 200 }, Qt::Vertical);
    }
    else if (dw->objectName() == "pythonShell")
    {
      dw->setVisible(true);
    }
    else
    {
      dw->setVisible(false);
    }
  }

  if ((attDW != nullptr) && (propDW != nullptr))
  {
    // mw->tabifyDockWidget(propDW, attDW);
    mw->tabifyDockWidget(attDW, propDW);
    propDW->setVisible(false);
    propDW = nullptr;
  }
  if ((attDW != nullptr) && (infoDW != nullptr))
  {
    // mw->tabifyDockWidget(propDW, attDW);
    mw->tabifyDockWidget(attDW, infoDW);
    infoDW->setVisible(false);
    infoDW = nullptr;
  }

  if (!set_auto_apply)
  {
    // Turn on auto-apply by default. This over-rides the preference.
    QAction* autoApplyAction = nullptr;
    Q_FOREACH (QAction* act, mw->findChildren<QAction*>())
    {
      if (act->objectName() == "actionAutoApply")
      {
        autoApplyAction = act;
        break;
      }
    }

    if (autoApplyAction && !autoApplyAction->isChecked())
    {
      autoApplyAction->activate(QAction::Trigger);
    }
    set_auto_apply = true;
  }
}
