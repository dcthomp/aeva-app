# We require aeva's version information to be included the CMake Project
# declaration in order to properly include aeva versions when using aeva as an
# SDK. We therefore do not have access to the variable ${PROJECT_SOURCE_DIR}
# at the time aevaVersion.cmake is read. We therefore use
# ${CMAKE_CURRENT_SOURCE_DIR}, since aevaVersion.cmake is called at the top
# level of aeva's build. We also guard against subsequent calls to
# aevaVersion.cmake elsewhere in the build setup where
# ${CMAKE_CURRENT_SOURCE_DIR} may no longer be set to the top level directory.

if (NOT DEFINED aeva_version)

  file(STRINGS ${CMAKE_CURRENT_SOURCE_DIR}/version.txt version_string )

  string(REGEX MATCH "([0-9]+)\\.([0-9]+)(\\.([0-9]+)[-]*(.*))?"
    version_matches "${version_string}")

  set(aeva_version_major ${CMAKE_MATCH_1})
  set(aeva_version_minor ${CMAKE_MATCH_2})
  # set(aeva_version_patch "${CMAKE_MATCH_3}")
  set(aeva_version "${aeva_version_major}.${aeva_version_minor}")
  # Do we just have a patch version or are there extra stuff?
  if (DEFINED CMAKE_MATCH_4)
    set(aeva_version_patch ${CMAKE_MATCH_4})
    set(aeva_version "${aeva_version}.${aeva_version_patch}")
  else()
    set(aeva_version_patch 0)
  endif()
  # To be thorough, we should split the label into "-prerelease+metadata"
  # and, if prerelease is specified, use it in determining precedence
  # according to semantic versioning rules at http://semver.org/ .
  # For now, just make the information available as a label:
  if (DEFINED CMAKE_MATCH_5)
    set(aeva_version_label "${CMAKE_MATCH_4}-${CMAKE_MATCH_5}")
  endif()
  # set(aeva_version "${aeva_version_major}.${aeva_version_minor}")

  set(aeva_version_minor_string ${aeva_version_minor})
  string(REGEX REPLACE "^0" "" aeva_version_minor_int ${aeva_version_minor})
endif()
