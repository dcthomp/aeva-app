set( CTEST_CUSTOM_MAXIMUM_NUMBER_OF_ERRORS 1000 )
set( CTEST_CUSTOM_MAXIMUM_NUMBER_OF_WARNINGS 1000 )

set(COVERAGE_EXTRA_FLAGS "-l -p")
set(CTEST_CUSTOM_COVERAGE_EXCLUDE
  # Exclude Omicron from coverage
  "Omicron"
  "vtk[^\\.]+ClientServer.cxx"
  "moc_[^\\.]+.cxx"
  "ui_[^\\.]+.h"
  "qrc_[^\\.]+.cxx"
  "pybind11"
  "smtk/thirdparty"
  "Testing"
  "testing"
  "autogen"
)
list(APPEND CTEST_CUSTOM_WARNING_EXCEPTION
  "tl_debug.*has no symbols"

  # https://gitlab.kitware.com/paraview/paraview/-/merge_requests/4237
  # https://gitlab.kitware.com/paraview/paraview/-/merge_requests/4240
  "pqaevaInitializer.cxx"
  "note: .* has been explicitly marked deprecated"
  "note: expanded from macro"

  # Ignore warning from CMB
  "cmb"

  # Ignore warnings from CMake autogen code
  "autogen"
  # ParaView generated code (paraview/paraview!4957)
  "AutoStartImplementation.*modernize-use-nullptr"
  "pqSMTKAutoStart.*Implementation.*modernize-use-nullptr"
  "Implementation.h.*misc-non-private-member-variables-in-classes"
  "Initializer.cxx.*readability-qualified-auto"
  # Fixes from paraview/paraview!5058
  "_server_manager\\.h.*misc-definitions-in-headers"
  "_server_manager\\.h.*modernize-use-emplace"
  "_server_manager_modules\\.h.*misc-definitions-in-headers"
  "_server_manager_modules\\.h.*modernize-use-emplace"
  "_client_server\\.h.*misc-definitions-in-headers"
  "_qch\\.h.*modernize-deprecated-headers"
  "_qch\\.h.*misc-definitions-in-headers"
  "_server_manager_modules_data\\.h.*misc-definitions-in-headers"
  "_server_manager_modules_data\\.h.*modernize-deprecated-headers"
  "_server_manager_data\\.h.*misc-definitions-in-headers"
  "_server_manager_data\\.h.*modernize-deprecated-headers"
  "note: make as 'inline'"

  # Warnings from sccache that we don't care about.
  "sccache: warning: The server looks like it shut down unexpectedly, compiling locally instead"
)
